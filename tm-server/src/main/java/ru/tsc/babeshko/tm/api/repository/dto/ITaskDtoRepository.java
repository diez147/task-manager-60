package ru.tsc.babeshko.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByUserId(@Nullable String userId);

}
