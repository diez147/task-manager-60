package ru.tsc.babeshko.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.model.UserDto;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    @Nullable
    UserDto findOneByLogin(@NotNull String login);

    @Nullable
    UserDto findOneByEmail(@NotNull String email);

}