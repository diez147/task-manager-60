package ru.tsc.babeshko.tm.api.repository.dto;

import ru.tsc.babeshko.tm.dto.model.ProjectDto;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

    void removeAllByUserId(String userId);

}
